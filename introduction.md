
## Introduction

========
### Théorie / Pratique

#### 1h / 1h


========
#### Un groupe de travail inter-disciplinaire

travaillant à

#### un portail de ressources, ouvert,
#### dédié à l'observation théorique et pratique des notebooks

[https://page.hn/k31zsl](https://page.hn/k31zsl)



========
### Portail de ressources

- développement "organique" (wiki & méthode [Zettelkasten](https://zettelkasten.de/posts/overview/))
- collaboratif ouvert ([Gitlab](https://gitlab.huma-num.fr/gt-notebook/wiki)), édition continue (Git)
- une  bibliographie partagée ([Zotero](https://www.zotero.org/groups/4416056/gt-notebooks))
- reproductible ([Nixos](https://nixos.org/))
- ressources → fiches théoriques et pratiques


========
### L'atelier du 3 décembre

- une co-construction très stimulante
- un premier jalon concret pour le GT
- des ancrages disciplinaires avec les problématiques "données"
- élargir aux discussions "métiers"
- Pad prise de notes collaboratives : https://page.hn/ulptrp

========
### Les notebooks : enjeux & opportunités


========
### Contexte

- **Science Ouverte** : injonctions institutionnelles
- **Données numériques** : méthodes quanti / quanti-quali

#### → Émergence de nouveaux modèles épistémiques


========
### nouvel objet éditorial scientifique

Paper | Data Paper | Executable Paper

> Nouvelle "publication scientifique" ou nouvelle "pratique de publication" ?

#### → déplacement de la publication
<!-- vers des objets intermédiaires, moins reconnus institutionnellement, mais plus opérationnels pour la communauté de recherche -->

========
### Opportunités
#### Collaboration
#### Littératie
#### Reproductibilité



========
### Programme
#### Théorie

- **Liens historiques**
- **Reproductibilité**
- **Le notebook comme objet technique**
- **Quelle place dans le cycle des données de la recherche ?**
- **Les initiatives**

<!-- .element: style="font-size:smaller;" -->

#### Pratique

- **TP 1 Notebook Pandémie**
- **TP 2 Notebook Wikidata**
- **Conclusion**

<!-- .element: style="font-size:smaller;" -->
