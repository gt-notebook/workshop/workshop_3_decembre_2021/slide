     
## Le notebook comme objet technique

========
### Le notebook au centre d’un système socio-technique
- Du langage informatique articulé avec du texte ?
- Une question d’intelligibilité des opérations réalisées par l’ordinateur & les méthodes associées ?

========

### Un objet technique porteur de différents objectifs et fonctions

- sensibilisation (une *profondeur de l’interface* du calculateur sur les données),
- enseignement, co-construction, accompagnement à la recherche,
- médiations, publications, etc.

<!-- .element: style="font-size:0.7em;" -->

> Un espace commun d’élaboration d’un discours scientifique (revue, traitement des données, publication, démonstration etc.)

<!-- .element: style="font-size:0.7em;" -->

========
<!-- .slide: data-background-color="white" -->
<img src="image/thumb-cyclevienotebook.png" style="width:850px;"/>


- des processus complexes 
- usage de sources et production de code 
- fonctions de représentation du réel (fondé sur des *données*)


<!-- .slide: data-background-color="white" -->
========
### Traduire *Notebook*</h3>

<img src="image/note_lecture_bloc_code_2.png" style="width:300px;"/>
<img src="image/note_lecture_bloc_code_1.png" style="width:300px;"/>

- un terme anglais polysémique
- y compris au sein d'une même communauté
- un objet décrit dans le cadre d'un paradigme d'ouverture de la recherche

========
### Des fonctions épistémiques support d'un dialogue entre disciplines ?
- faciliter (I) l'expérience et l'observation contrôlée, (II) la formulation intelligible, (III) la théorisation, (V) la décision et l'action
- **faciliter la construction des savoirs (IV)**
