# Support d'atelier [<img src="image/logo_gt_notebook.png" align="right" width="200"/>](https://gt-notebook.gitpages.huma-num.fr/site-hugo/)
### Les Notebooks, enjeux et problématiques en lien avec le cycle des données de la recherche
#### Journée d'études normande sur les données de la recherche - 3/12/2021 (Caen)

**Konrad HINSEN** (Centre de Biophysique Moléculaire et Synchrotron SOLEIL, CNRS)    
**Raphaëlle KRUMMEICH** (IRIHS, Univ. de Rouen Normandie)     
**Laurent MOUCHARD** (LITIS, Univ. de Rouen Normandie)    
**Hugues PECOUT** (FR CIST, CNRS)     
**Pierre POULAIN** (IJM, Université de Paris)       
**Sébastien REY-COYREHOURCQ** (IDEES, Univ. de Rouen Normandie)    
**Nicolas SAURET** (TGIR Humanum - HNLab)    

<br/>  

[**-> CONSULTER**](https://gt-notebook.gitpages.huma-num.fr/workshop/workshop_3_decembre_2021/slide)

<br/>  

### Charte graphique/proposition
Partant du programme PDF de la JE SDDN
*bleu* 
```background-color : #263A54;  
background-color : rgb(38,58,84); 
background-color : hsl(214,38%,24%);
```

### Plan détaillé
Lien vers les slides reveal.js : https://gitlab.huma-num.fr/gt-notebook/workshop/workshop-gtdonnees-nu    
Lien vers les CR : https://codimd.univ-rouen.fr/cZSHaf0NSdKkMW5ziauxDQ    
Lien vers l'issue pour les discussions : https://gitlab.huma-num.fr/gt-notebook/roadmap/-/issues/9    
Lien vers le billet/[Note de lecture “Du notebook au bloc-code”, Arthur Perret, juin 2021](https://codimd.univ-rouen.fr/dk2h94SbRdqgdoYaEaiLNQ#) - Traduire le *notebook* entre "bloc-code" et "document-outil"... une *marge d'indétermination* ? une multiplicité de *tables d'opérations* ?      
Voir ce qui est reproductible et ce qui ne l'est pas (MOOC Konrad, Chapitre 2 : Six catégories de reproductibilité, [vidéo](https://www.youtube.com/watch?v=LKVCKTJyKlo)) par exemple.



#### 0. Introduction (Nicolas? et Al.)

#### 1. Liens historiques (Sebastien & Al.)

- Qu'est ce que le LP 
   - voir https://gitlab.huma-num.fr/gt-notebook/roadmap/-/issues/11
   
- Les Notebooks 
    - expression +/- complète du LP (diagramme Venn) 
        - Observable != org-mode != RMarkdown != Jupyter != ... en différents points/axes que l'on peut essayer d'identifier pour ensuite les classer
        - => typologie formcément incomplète, renvoi au wiki pour plus de détails sur les autres outils 
    - porteur de sa propre histoire, l'existence est ancienne (Mapple, Mathematica, etc.) Une ref intéressante : https://www.theatlantic.com/science/archive/2018/04/the-scientific-paper-is-obsolete/556676/
    - Par rapport au LP seul, extensions souhaitable pour les usages actuels : 
        - HPC (High Performance Computing), ie Délégation de calculs ? ( ex Pangéo, https://pangeo.io/setup_guides/hpc.html)
        - Lien avec les entrepôts de données & Moissonage ? (ex Pangéo )
        - Interactivité / Dashboard

#### 2. Reproductibilité (Konrad et al.)

- préalable... objet technique / limites & adaptation au projet de recherche & ses données (coûts associés) - Konrad / 
    https://gitlab.huma-num.fr/gt-notebook/roadmap/-/issues/12
    
#### 3. Epistémologie de l'objet (Raphaelle et al.  )

3.1 Le notebook au centre d'un système/processus socio-technique *émergent*

- *du langage informatique articulé (& visible ou lisible ?) à du texte*)  
- une question d'*intelligibilité* des opérations réalisées par l'ordinateur & méthodes ?  

3.2 Un objet porteur de différents objectifs ou fonctions, qui peuvent se recouper, de façon volontaire ou involontaire  

- sensibilisation (une *profondeur* de l'interface du *calculateur*), enseignement,   
- co-construction, accompagnement d'une transformation des pratiques *par le numérique*,  
- médiations, publications, etc.  
- un espace commun d'élaboration d'un discours scientifique (NB *un discours sans auteur*)

> Quelques exemples de nbook à fonctions différenciées (revue digitalhistory, nb Michael Nauge, nb ScePo, [nb Kembellec](http://geraldkembellec.scienceontheweb.net/notebooks/adressbuch.html)) 

> voir papier Pierre (Zika + analyse de milliers de notebooks)  

3.3 Traduire *notebook*

- bloc-code, document-outil, artycle etc. : des désignations du *notebook* signes de la multiplicité des usages/ non-usages & fonctions  
- recherche entrain de se faire ou produit fini/datapaper ? - les limites dans la pratique des *datascientists* [@chattopadhyay2020whats](https://gitlab.huma-num.fr/gt-notebook/wiki/-/blob/main/notes/20211109154112.md)  

3.4 Fonctions epistémiques

5 grandes catégories (voir Varenne, 2017, le cas de la géographie)
- faciliter l'expérience et l'observation contrôlée
- faciliter la formulation intelligible
- faciliter la théorisation
- faciliter la construction des savoirs
	- faciliter la communication entre acteurs scientifiques
	- faciliter la délibération et la concertation entre parties prenantes
	- faciliter la co-construction de représentations et de modes de contrôle de systèmes mixtes (humain, physique, environnement) en vue d'une gestion concertée et non top-down ni centralisée
- faciliter la décision et l'action

#### 4. Quelle place dans le cycle des données de la recherche (Sebastien et Al.)

De part sa capacité à concentrer en un lieu différents objectifs en terme de fonctions épistémiques et/ou d'usages, le "Notebook" est loin d'être un objet simple à cerner, sur lui pèse de nombreuses contraintes, qui se recoupent.

voir https://gitlab.huma-num.fr/gt-notebook/roadmap/-/issues/13

#### 5. Les initiatives (Pierre? et Al.)

Quelques slides sur les initiatives de plateforme qui se mettent en place, 
les avantages, les limites : 

- A Paris et à Rouen : Laurent, Pierre Bruno, ?
- A Humanum avec Calisto ? Göttingen
- Autres : Pangéo ? (https://pangeo.io/index.html) , IFB => solution technologiques possible / contexte scientifique pluri disciplinaire
- Rzine ? (hugues/seb)

#### 6. Présentation du GT & Discussions (Sebastien & Al.)


#### 7. TP - Storytelling (Hugues et Al.)

7.1 TP1 - Python (Pierre/Raphaëlle et Al.)

7.2 TP2 - R (Hugues/Raphaëlle et Al.)


#### 8. Conclusion


