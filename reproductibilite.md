## Reproductibilité

========
### La reproductibilité computationnelle

Un résultat (chiffre, tableau, figure, ...) est <i>reproductible</i>
si on peut refaire le calcul qui l'a produit et trouver un résultat
identique.

========
### Avantages

- On sait exactement ce qui a été fait
- On peut examiner le code et les données
- On peut expérimenter pour mieux comprendre
- On peut réutiliser le code et/ou les données

========
### Pas de garant d'exactitude

- Un résultat reproductible peut être faux, imprécis, inapproprié, ...
- C'est un label de qualité, ni plus, ni moins.


#### Notebook ⇏ reproductible

> the authors empirically find that only 24.11 % of their
> selected notebooks can be executed without errors, and only
> 4.03% of them can produce the same results, compared to that
> of the original outputs of the notebooks.

<!-- .element: style="font-size:smaller;" -->
JF Pimentel, L Murta, V Braganholo, J Freire,
“A Large-Scale Study About Quality and Reproducibility of Jupyter Notebooks.”
https://doi.org/10.1109/MSR.2019.00077.
<!-- .element: style="font-size:smaller;" -->


========
### La pile logicielle en-dessous du notebook</h3>

<img src="image/pile-logicielle.svg" style="width:650px;" />

========
### Environnements de calcul

- Définition : toute la pile en-dessous du notebook
- Typiquement des centaines de logiciels distincts
- Deux approches complémentaires
 + archiver
 + rendre reproductible


========
### Environnements de calcul archivés

- Facile à utiliser
- Difficile de savoir ce qu'il y a dedans
- Gros fichiers à stocker
- Outils : Docker, Binder

========
### Environnements de calcul reproductibles

- Transparents, modifiables
- Compacts
- Parfois difficiles à construire
- Outils : Guix, Nix



   